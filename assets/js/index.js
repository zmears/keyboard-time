$(() => {

    $("#new-user").on('click', newUser);
    $("#in").on('click', timeIn);
    $("#out").on('click', timeOut);
    $("#users").on('change', changeUser);

    let user = $("#time-buttons").data('user');


    function newUser() {
        let name = prompt("Name");
        if (name !== "") {
            $.post('/user', {name: name})
                .done(function (response) {
                    window.location = response.url;
                });
        }
    }

    function changeUser() {
        window.location = `/${$(this).val()}`;

    }

    function timeIn() {
        $.post(`/mark/${user}/in`)
            .done(function (response) {
                window.location.reload();
            });
    }
    
    function timeOut() {
        $.post(`/mark/${user}/out`)
            .done(function (response) {
                window.location.reload();
            });
    }

});
