let Encore = require('@symfony/webpack-encore');

Encore
// the project directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // the public path used by the web server to access the previous directory
    .setPublicPath('/build')
    .cleanupOutputBeforeBuild()
    .enableSassLoader()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())

    .addEntry('js/chart', 'chart.js')
    .addEntry('js/index', './assets/js/index.js')

    .autoProvidejQuery()
    .autoProvideVariables({
        "window.jQuery": "jquery", //Allows you to use jquery in any file
        "window.chart": "chart",
    })
    //These files are shared across the entire app
    .createSharedEntry('vendor', [
        'jquery',
        'jquery-ui-bundle',
        'bootstrap',
        '@fortawesome/fontawesome',
        '@fortawesome/fontawesome-free-brands',
        '@fortawesome/fontawesome-free-regular',
        '@fortawesome/fontawesome-free-solid',
        'bootstrap/scss/bootstrap.scss',
        'jquery-ui-bundle/jquery-ui.css'
    ])
;

module.exports = Encore.getWebpackConfig();
