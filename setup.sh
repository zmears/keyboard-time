#!/usr/bin/env bash

# Initial setup of the project
requiredPhpVersion='7.1'

initialChecks() {
    errors=false

    # Gotta have PHP installed and in your path
    if ! command -v php >/dev/null 2>&1; then
        # If PHP isn't available, we can't contine
        echo 'You need PHP to run this script.' >&2

        # Set errors to true
        errors=true
    else
        # If PHP is installed, we'll set $php to the executable
        php=$(command -v php)
    fi

    if ! ${php} -r "version_compare(phpversion(), '$requiredPhpVersion', '>=') ? exit(0) : exit(1);"; then
        # If PHP isn't at least $requiredPhpVersion, we can't continue
        echo "You need at least PHP $requiredPhpVersion to use this script." >&2

        # Set errors to true
        errors=true
    fi

    # Gotta have Git installed and in your path
    if ! command -v git >/dev/null 2>&1; then
        # If Git isn't available, we can't continue
        echo 'You need Git installed to use this script.' >&2

        # Set errors to true
        errors=true
    else
        # If Git is installed, we'll set $git to the executable
        git=$(command -v git)
    fi

    # Need NPM installed; dev only for now
    if [ "${APP_ENV}" == 'dev' ] || [ "${APP_ENV}" == 'build' ] || [ "${APP_ENV}" == 'qa' ]; then
        if ! command -v npm >/dev/null 2>&1; then
            echo 'You need NPM installed to use this script in dev or build'

            errors=true
        else
            npm=$(command -v npm)
        fi
    fi

    if [ "${APP_ENV}" == 'build' ] || [ "${APP_ENV}" == 'qa' ] || [ "${APP_ENV}" == 'prod' ]; then
        if ! command -v aws >/dev/null 2>&1; then
            echo 'You need the AWS cli installed to use this script in build and production.'

            errors=true
        else
            aws=$(command -v aws)
        fi
    fi

    # If there were any errors, return false. Otherwise, return true
    # This method will let us add more checks later on
    if ${errors}; then
        return 1
    fi

    return 0
}

trapUser() {
    # Trap user interupts
    # In case we run into a need to handle them later
    printf '\n\nTerminated by user!\n\n'
    exit 1
}

trapGeneral() {
    # Trap other errors
    # In case we run into a need to handle them later
    printf '\n\nUnexpected problem encountered!\n\n' >&2
    exit 1
}

doNpmDance() {
    # Install NPM packages
    ${npm} install


    if [ "${APP_ENV}" == 'build' ]; then
        ./node_modules/.bin/encore production
    else
        ./node_modules/.bin/encore dev
    fi
}

clearCache() {
    # Clear the cache (hardcore mode)
    echo 'Clearing cache...' >&2
    rm -rf var/cache/*
}

migrationStatus() {
    bin/console doctrine:migrations:status
}

getManifest() {
    latestHash="$(git rev-parse HEAD)"
    s3Url="s3://tmg-oneview-assets/manifest.$latestHash.json"

    echo "Downloading Manifest: $s3Url" >&2

    ${aws} s3 cp ${s3Url} public/build/manifest.json
}

# Trap user interupts
trap trapUser INT QUIT

# Trap other trappable interupts
trap trapGeneral HUP TERM


#Souce the env file if present
if [ -e ./.env ]; then
    echo '.env file found, sourcing'
    source ./.env
fi

#Ensure we have an APP_ENV set
if [[ -z "${APP_ENV}" ]]; then
  echo "APP_ENV is not defined"

  exit 1;
fi

echo "Setting up the env for ${APP_ENV}"

#Check for
initialChecks

case "${APP_ENV}" in
    dev | qa | build)
        composer install
        clearCache
        doNpmDance
        migrationStatus
    ;;
    prod)
        composer install
        clearCache
        getManifest
        migrationStatus
    ;;
esac
