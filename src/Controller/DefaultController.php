<?php

namespace App\Controller;

use App\Entity\User;
use App\Service\TimeService;
use App\Service\UserService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{

    /**
     * @Route("/", name="index")
     */
    public function index(UserService $userService)
    {
        $users = $userService->all();

        if (!empty($users)) {
            return $this->redirectToRoute('user_time', ['id' => current($users)->getId()]);
        }

        return $this->render('index.html.twig', ['users' => $users, 'user' => null, 'time' => null]);
    }

    /**
     * @Route("/user", name="new_user", methods={"POST"})
     *
     * @param Request $request
     * @param UserService $userService
     */
    public function newUser(Request $request, UserService $userService)
    {
        $user = $userService->new($request->get('name'));
        return $this->json(['success' => true, 'url' => $this->generateUrl('user_time', ['id' => $user->getId()])]);
    }

    /**
     * @Route("/mark/{id}/{action}", name="mark_time")
     *
     * @param User $user
     * @param $action
     */
    public function mark(User $user, $action, TimeService $timeService)
    {
        $timeService->logTime($user, $action);

        return $this->json(['success' => true]);
    }

    /**
     * @Route("/{id}", name="user_time")
     *
     * @param User $user
     * @param UserService $userService
     *
     * @return Response
     */
    public function userTime(User $user, UserService $userService, TimeService $timeService)
    {
        return $this->render('index.html.twig', ['users' => $userService->all(), 'user' => $user, 'time' => $timeService->activeTime($user)]);
    }
}
