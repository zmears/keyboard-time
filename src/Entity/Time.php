<?php

namespace App\Entity;

use App\Traits\TimestampedTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="Time")
 * @ORM\Entity(repositoryClass="App\Repository\TimeRepository")
 * @ORM\HasLifecycleCallbacks;
 */
class Time
{
    use TimestampedTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="time")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $user;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $timeIn;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $timeOut;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $total;

    /**
     * Time constructor.
     *
     * @param User|null $user
     */
    public function __construct(?User $user)
    {
        $this->setUser($user);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getTimeIn(): ?\DateTimeInterface
    {
        return $this->timeIn;
    }

    /**
     * @param \DateTimeInterface $timeIn
     *
     * @return Time
     */
    public function setTimeIn(\DateTimeInterface $timeIn): self
    {
        $this->timeIn = $timeIn;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getTimeOut(): ?\DateTimeInterface
    {
        return $this->timeOut;
    }

    /**
     * @param \DateTimeInterface|null $timeOut
     *
     * @return Time
     */
    public function setTimeOut(?\DateTimeInterface $timeOut): self
    {
        $this->timeOut = $timeOut;

        if ($this->timeIn && $this->timeOut) {
            $this->setTotal($this->timeOut->getTimestamp() - $this->timeIn->getTimestamp());
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getTotal(): ?int
    {
        return $this->total;
    }

    /**
     * @param int $total
     *
     * @return Time
     */
    public function setTotal(int $total): self
    {
        $this->total = $total;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return Time
     */
    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return string
     */
    public function totalPretty(): string
    {
        return sprintf('%02d:%02d:%02d', ($this->getTotal()/3600),($this->getTotal()/60%60), $this->getTotal()%60);
    }
}
