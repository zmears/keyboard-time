<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="User")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @var Collection|Time[]
     *
     * @ORM\OneToMany(targetEntity="Time", mappedBy="user")
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $time;

    /**
     * User constructor.
     *
     * @param null $name
     */
    public function __construct($name = null)
    {
        $this->setName($name);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return User
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Time[]|Collection
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param Time $time
     *
     * @return User
     */
    public function addTime(Time $time): self
    {
        $this->time->add($time);

        return $this;
    }

    /**
     * @param Time[]|Collection $time
     *
     * @return User
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }
}
