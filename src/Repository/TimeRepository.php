<?php

namespace App\Repository;

use App\Entity\Time;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Time|null find($id, $lockMode = null, $lockVersion = null)
 * @method Time|null findOneBy(array $criteria, array $orderBy = null)
 * @method Time[]    findAll()
 * @method Time[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TimeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Time::class);
    }

    /**
     * Get latest time record
     *
     * @param User $user
     *
     * @return Time|null
     */
    public function lastTime(User $user)
    {
        return $this->createQueryBuilder('time')
            ->where('time.user = :user')
            ->andWhere('time.total is null')
            ->setParameter('user', $user)
            ->orderBy('time.id', Criteria::DESC)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

}
