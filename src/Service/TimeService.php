<?php

namespace App\Service;

use App\Entity\Time;
use App\Entity\User;

class TimeService extends BaseService
{
    public function logTime(User $user, $action)
    {
        $time = $this->activeTime($user) ?? new Time($user);
        switch ($action) {
            case 'in':
                $time->setTimeIn(new \DateTime());
                break;
            case 'out':
                $time->setTimeOut(new \DateTime());
                break;
            default:
                throw new \Exception('Invalid action'.$action);
        }

        $this->save($time);
    }

    /**
     * @param User $user
     *
     * @return Time|null
     */
    public function activeTime(User $user)
    {
        return $this->entityManager->getRepository(Time::class)->lastTime($user);
    }

}
