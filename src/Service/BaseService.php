<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;

class BaseService
{
    /** @var EntityManagerInterface */
    protected $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param mixed $entity
     */
    protected function save($entity)
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
    }
}
