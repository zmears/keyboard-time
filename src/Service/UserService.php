<?php

namespace App\Service;

use App\Entity\User;
use Doctrine\Common\Collections\Criteria;

class UserService extends BaseService
{
    /**
     * Create a new user
     *
     * @param $name
     *
     * @return User
     */
    public function new($name)
    {
        $user = new User($name);

        $this->save($user);

        return $user;
    }

    /**
     * Get all the users
     *
     * @return User[]|array
     */
    public function all()
    {
        return $this->entityManager->getRepository(User::class)->findBy([], ['name' => Criteria::ASC]);
    }
}
